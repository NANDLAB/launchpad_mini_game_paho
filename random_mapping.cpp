#include "random_mapping.hpp"
#include <cassert>
#include <cstdlib>
#include <ctime>

random_mapping::random_mapping(std::uint8_t size)
    : map(size), imap(size)
{
    for (std::uint8_t i = 0; i < size; i++) {
        imap[i] = map[i] = i;
    }
}

std::uint8_t random_mapping::f(std::uint8_t x) {
    assert(x < map.size());
    return map[x];
}

std::uint8_t random_mapping::g(std::uint8_t y) {
    assert(y < imap.size());
    return imap[y];
}

void random_mapping::shuffle() {
    using namespace std;
    const uint8_t size = map.size();
    const uint8_t empty = 0xFF;
    imap =  vector<uint8_t>(size, empty); // inverse map
    uint8_t r, y;
    for (uint8_t x = 0; x < size; x++) {
        if (x == size - 1) {
            r = 0;
        }
        else {
            r = rand() % (size - x);
        }
        y = 0;
        do {
            while (r && imap[y] == empty) {
                y++;
                r--;
            }
            while (imap[y] != empty) {
                y++;
            }
        }
        while (r);
        map[x] = y;
        imap[y] = x;
    }
}
