#include <iostream>
#include <boost/asio/io_context.hpp>
#include "launchpad_mini_game_paho.hpp"
#include <exception>
#include <cstdlib>
#include <chrono>
#include <boost/program_options.hpp>

#define HELP "help"
#define VERSION "version"
#define CFG "config"

#define SF "synth.soundfont"
#define DEV "synth.device"
#define MID "song.mid"
#define CHAN "song.channel"
#define LKEY "song.lowkey"
#define NODE "mqtt.node"
#define BROKER "mqtt.broker"
#define LPIN "launchpad.in"
#define LPOUT "launchpad.out"

int main(int argc, char *argv[])
{
    try {
        namespace po = boost::program_options;
        using std::cout;
        using std::cerr;
        using std::endl;
        using std::ifstream;
        typedef std::string str;
        typedef unsigned uns;

        po::options_description generic("Generic options");
        generic.add_options()
                (HELP ",h", "produce help message")
                (VERSION ",v", "print version string")
                (CFG ",g", po::value<str>()->value_name("FILE"), "configuration file")
        ;

        po::options_description config("Configuration");
        config.add_options()
                (SF ",s", po::value<str>()->value_name("FILE")->required(), "soundfont for the synthesizer")
                (DEV ",d", po::value<str>()->value_name("FILE")->default_value("default"), "audio device for the synthesizer output")
                (MID ",m", po::value<str>()->value_name("FILE")->required(), "midi song file")
                (CHAN ",c", po::value<uns>()->value_name("NUM")->default_value(0), "song channel to play")
                (LKEY ",l", po::value<uns>()->value_name("NUM")->default_value(32), "lowest key which maps to a pad")
                (NODE ",n", po::value<str>()->value_name("NAME")->default_value("launchpad"), "mqtt node name")
                (BROKER ",b", po::value<str>()->value_name("URI")->default_value("localhost"), "mqtt broker uri")
                (LPIN ",i", po::value<uns>()->value_name("NUM")->default_value(2), "launchpad midi input port")
                (LPOUT ",o", po::value<uns>()->value_name("NUM")->default_value(2), "launchpad midi output port")
        ;

        po::options_description cmdline_opts;
        cmdline_opts.add(generic).add(config);

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, cmdline_opts), vm);

        str progname = ((argc >= 1) ? argv[0] : "NONAME");

        if (vm.count(HELP)) {
            cout << "Usage: " << progname << " OPTION...\n" << cmdline_opts << endl;
            return 0;
        }

        if (vm.count(VERSION)) {
            cout << progname << " no version number yet" << endl;
            return 0;
        }

        if (vm.count(CFG)) {
            const str &cfg_file = vm[CFG].as<str>();
            ifstream cfg_stream(cfg_file);
            if (cfg_stream) {
                po::store(boost::program_options::parse_config_file(cfg_stream, config), vm);
            }
            else {
                cerr << "Error: Cannot open " << cfg_file << endl;
                return 1;
            }
        }

        po::notify(vm);

        boost::asio::io_context ioc;
        std::srand(std::chrono::system_clock::now().time_since_epoch().count());

        launchpad_mini_game_paho game(
                    ioc, vm[NODE].as<str>(), vm[BROKER].as<str>(), vm[LPIN].as<uns>(), vm[LPOUT].as<uns>(),
                    vm[MID].as<str>(), vm[CHAN].as<uns>(), vm[LKEY].as<uns>(), vm[SF].as<str>(), vm[DEV].as<str>());
        game.start();
        ioc.run();
        return 0;
    }
    catch (const std::exception &e) {
        std::cerr << "Terminating due to exception: " << e.what() << std::endl;
        return 1;
    }
}
