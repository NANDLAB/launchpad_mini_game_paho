#ifndef LAUNCHPAD_MINI_GAME_PAHO_HPP
#define LAUNCHPAD_MINI_GAME_PAHO_HPP

#include <memory>
#include "mqtt_node_paho.hpp"
#include "LaunchpadMiniAsync.hpp"
#include "fluid_auto.hpp"
#include "random_mapping.hpp"
#include "MidiFile.h"

/**
 * @brief The launchpad_mini_game_paho class
 * Senso-like game on the Launchpad Mini.
 * A MIDI song file is played and different pads light up
 * according to the notes. The audio is synthesized
 * with fluidsynth.
 * The player has to repeat the notes.
 * The program receives mqtt control signals from the admin
 * like "activate", "reset".
 * When the player completes the game, a "solved" mqtt message
 * is sent out.
 */
class launchpad_mini_game_paho : public mqtt_node_paho
{
    static const std::string signal_activate;
    static const std::string signal_reset;
    static const std::string topic_gameevent;
    static const std::string gameevent_solved;

    void clean_exit() override;

    const std::string full_topic_gameevent;

    const unsigned port_in;
    const unsigned port_out;
    novation::LaunchpadMiniAsync launchpad;

    const std::string midi_filename;
    smf::MidiFile midi_file;
    const unsigned midi_channel;
    const unsigned lowkey;

    fluid_settings_auto fluid_settings;
    fluid_synth_auto fluid_synth;
    fluid_audio_driver_auto fluid_audio_driver;
    std::string soundfont;
    std::string audio_device;

    /**
     * Game state constants to be used for the gamestate vector.
     */
    enum {
        IDLE = 0,
        RUNNING,

        MENU = 0,
        INGAME,

        STARTSCREEN = 0,
        LOADING,
        TRYAGAIN,

        PROGRESS = 0,
        WIN,

        GUIDE = 0,
        PRERELEASE,
        REPEAT,
        POSTRELEASE,
        FAILED,
        COMPLETED,

        GREENON = 0,
        GREENOFF,

        REDON = 0,
        REDOFF
    };

    /**
     * @brief gamestate
     * Vector which defines the game state hierarchically.
     * It can change length during runtime (always at least one).
     * The first element is the most significant
     * and the other ones specify more and more details.
     * <LEVEL> is the number of midi events currently played,
     * it can skip some values for events like program change.
     * <EVENT> is the midi event number played right now.
     *
     * IDLE                                                : waiting for an activate signal
     * RUNNING.MENU.STARTSCREEN                            : showing the startscreen
     * RUNNING.MENU.LOADING                                : short delay before actually starting the game
     * RUNNING.MENU.TRYAGAIN                               : try again animation
     * RUNNING.INGAME.PROGRESS.<LEVEL>.GUIDE.<EVENT>       : the program guides how to play the song
     * RUNNING.INGAME.PROGRESS.<LEVEL>.PRERELEASE          : waiting for the player to release all pads (before REPEAT)
     * RUNNING.INGAME.PROGRESS.<LEVEL>.REPEAT.<EVENT>      : the player has to repeat the shown notes
     * RUNNING.INGAME.PROGRESS.<LEVEL>.POSTRELEASE         : waiting for the player to release all pads (after REPEAT)
     * RUNNING.INGAME.PROGRESS.<LEVEL>.FAILED.<PAD>.REDON  : the player pressed the wrong pad <PAD>
     * RUNNING.INGAME.PROGRESS.<LEVEL>.FAILED.<PAD>.REDOFF : turn off red LED light
     * RUNNING.INGAME.PROGRESS.<LEVEL>.COMPLETED.GREENON   : green LED shows level completion
     * RUNNING.INGAME.PROGRESS.<LEVEL>.COMPLETED.GREENOFF  : green LED turns off
     * RUNNING.INGAME.WIN                                  : win animation
     */
    std::vector<std::size_t> gamestate;

    boost::asio::steady_timer gametimer;

    typedef std::chrono::duration<double> doublesec;

    std::chrono::steady_clock::time_point start_time;

    random_mapping key_pad_map;

    std::vector<std::vector<uint8_t>> key_state;

    void gametimer_timeout(const boost::system::error_code &err);

    /**
     * @brief start_guiding
     * Play the first midi event and set gametimer expiry for the next event.
     * Adjust the gamestate to GUIDE.
     * @param level
     */
    void start_guiding(std::size_t level);

    /**
     * @brief wait_for_next_midi_event
     * Set gametimer expiry for the next midi event (used for GUIDE).
     * @param next_event
     */
    void wait_for_next_midi_event(std::size_t next_event);

    /**
     * @brief start_repeat
     * Find the first NOTE_ON event on the midi_channel and set state to REPEAT.
     * Assumes that the gamestate already has the following form:
     * RUNNING.INGAME.PROGRESS.<LEVEL>... (e.g. GUIDE, RELEASEPADS)
     * @param level
     */
    void start_repeat();

    /**
     * @brief find_first_note_on
     * @param from - at which event index to start searching
     * Iterates through the midi_channel events, until a NOTE_ON is found.
     * This method assumes there are NOTE_ON events, otherwise the behavior is undefined.
     * In the mean time, all patch changes are applied.
     * @return the index of the found NOTE_ON event
     */
    std::size_t find_first_note_on(std::size_t from);

    void midiCallback(novation::Pad pad, std::uint8_t press);

    void play_midi_event(const smf::MidiEvent &e);

    void reset();

    /**
     * @brief key2pad
     * @param midi key
     * @return launchpad white pad
     */
    novation::Pad key2pad(int key);

    /**
     * @brief pad2key
     * @param launchpad white pad
     * @return midi key
     */
    int pad2key(novation::Pad pad);

protected:
    bool interpret_message(mqtt::const_message_ptr msg) override;

public:
    launchpad_mini_game_paho(
            boost::asio::io_context &ioc,
            std::string node_name,
            std::string broker_uri,
            unsigned port_in,
            unsigned port_out,
            std::string midi_filename,
            unsigned midi_channel,
            unsigned lowest_key,
            std::string soundfont,
            std::string audio_device);
    ~launchpad_mini_game_paho() override;
    void start() override;
};

#endif // LAUNCHPAD_MINI_GAME_PAHO_HPP
