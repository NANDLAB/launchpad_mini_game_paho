#ifndef MIDIFILE_EXCEPTION_HPP
#define MIDIFILE_EXCEPTION_HPP

#include <exception>


class midifile_exception : public std::exception
{
public:
    const char * what() const noexcept override;
};

#endif // MIDIFILE_EXCEPTION_HPP
