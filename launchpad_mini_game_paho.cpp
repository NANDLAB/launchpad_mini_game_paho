#include "launchpad_mini_game_paho.hpp"
#include <boost/bind/bind.hpp>
#include <iomanip>
#include "midifile_exception.hpp"
#include <boost/asio/placeholders.hpp>

using namespace boost::placeholders;
using namespace novation;
namespace chrono = std::chrono;

typedef launchpad_mini_game_paho game;
typedef mqtt_node_paho super;
using std::uint8_t;
using std::size_t;
using std::vector;
using std::make_shared;
using std::move;
using std::string;
using std::cout;
using std::endl;
using smf::MidiEventList;
using smf::MidiEvent;
using novation::Pad;

const std::string game::signal_activate("ACTIVATE");
const std::string game::signal_reset("RESET");

const std::string game::topic_gameevent("gameevent");
const std::string game::gameevent_solved("SOLVED");

game::launchpad_mini_game_paho(
        boost::asio::io_context &ioc,
        string node_name,
        string broker_uri,
        unsigned port_in,
        unsigned port_out,
        string midi_filename,
        unsigned midi_channel,
        unsigned lowest_key,
        string soundfont,
        string audio_device)
    : super(ioc, move(node_name), move(broker_uri)),
      full_topic_gameevent(full_topic(topic_gameevent)),
      port_in(port_in),
      port_out(port_out),
      launchpad(ioc),
      midi_filename(move(midi_filename)),
      midi_file(),
      midi_channel(midi_channel),
      lowkey(lowest_key),
      fluid_settings(),
      fluid_synth(fluid_settings),
      fluid_audio_driver(),
      soundfont(soundfont),
      audio_device(move(audio_device)),
      gamestate{IDLE},
      gametimer(ioc),
      start_time(),
      key_pad_map(64),
      key_state(16, vector<uint8_t>(128, 0))
{}

game::~launchpad_mini_game_paho() {
    launchpad.setCallback(0);
    launchpad.close();
}

static uint8_t white_pad_to_num(Pad pad) {
    return pad.x + pad.y * 8;
}

static Pad num_to_white_pad(uint8_t padnum) {
    return Pad(padnum % 8, padnum / 8);
}

void game::midiCallback(Pad p, uint8_t press) {
    using namespace std;
    std::cout << "("
              << std::setw(2) << unsigned(p.x) << ","
              << std::setw(2) <<  unsigned(p.y) << ") : "
              << unsigned(press) << std::endl;
    std::uint8_t padnum = p.padnum();
    if (gamestate[0] == RUNNING) {
        assert(gamestate.size() > 1);
        if (gamestate[1] == MENU && press) {
            assert(gamestate.size() > 2);
            if (gamestate[2] == STARTSCREEN) {
                launchpad.setSysExTextScrollOff();
                gametimer.expires_after(std::chrono::milliseconds(1000));
                gametimer.async_wait(boost::bind(&game::gametimer_timeout, this, boost::asio::placeholders::error));
                gamestate[2] = LOADING;
            }
            else if (gamestate[2] == TRYAGAIN) {
                launchpad.setSysExTextScrollOff();
                gametimer.expires_after(std::chrono::milliseconds(1000));
                gametimer.async_wait(boost::bind(&game::gametimer_timeout, this, boost::asio::placeholders::error));
                gamestate[2] = LOADING;
            }
        }
        else if (gamestate[1] == INGAME) {
            assert(gamestate.size() > 2);
            // During progress, only white pads are relevant.
            if (gamestate[2] == PROGRESS && p.x < 8 && p.y < 8) {
                assert(gamestate.size() > 4);
                size_t level = gamestate[3];
                int pressed_key = pad2key(p); // or released
                if (gamestate[4] == PRERELEASE) {
                    if (launchpad.getWhitePressedNone()) {
                        cout << debug_prefix() << "All white pads released, starting REPEAT phase." << endl;
                        start_repeat();
                    }
                }
                else if (gamestate[4] == REPEAT) {
                    assert(gamestate.size() > 5);
                    cout << "X: " << int(p.x) << ", Y: " << int(p.y) << endl;
                    cout << pressed_key << endl;
                    size_t e = gamestate[5];
                    MidiEvent event = midi_file[0][e];
                    assert(event.isNoteOn());
                    int expected_key = event.getKeyNumber();
                    if (press) {
                        if (pressed_key == expected_key) {
                            key_state[midi_channel][pressed_key] = event.getVelocity();
                            fluid_synth.noteon(midi_channel, pressed_key, event.getVelocity());
                            launchpad.setLightSingle(p, make_shared<ConstLightStatic>(event.getVelocity()));
                            if (e >= level) {
                                cout << "Level completed, waiting for the white pads to be released..." << endl;
                                gamestate = {RUNNING, INGAME, PROGRESS, level, POSTRELEASE};
                            }
                            else {
                                cout << "e is " << e << endl;
                                size_t next_note_on = find_first_note_on(e + 1);
                                gamestate = {RUNNING, INGAME, PROGRESS, level, REPEAT, next_note_on};
                            }
                        }
                        else {
                            fluid_synth.all_notes_off(-1);
                            fluid_synth.noteon(midi_channel, pressed_key, 0x50);
                            launchpad.reset();
                            launchpad.setLightSingle(p, make_shared<ConstLightStatic>(0x48));
                            cout << "Wrong pad" << endl;
                            gametimer.expires_after(chrono::milliseconds(1500));
                            gametimer.async_wait(boost::bind(&game::gametimer_timeout, this, boost::asio::placeholders::error));
                            gamestate = {RUNNING, INGAME, PROGRESS, level, FAILED, white_pad_to_num(p), REDON};
                        }
                    }
                    else {
                        if (key_state[midi_channel][pressed_key]) {
                            key_state[midi_channel][pressed_key] = 0;
                            fluid_synth.noteoff(midi_channel, pressed_key);
                            launchpad.setLightSingle(p, make_shared<ConstLightStatic>(0x00));
                        }
                    }
                }
                else if (gamestate[4] == POSTRELEASE) {
                    /*
                    if (press) {
                        fluid_synth.all_notes_off(-1);
                        fluid_synth.noteon(midi_channel, pressed_key, 0x50);
                        launchpad.reset();
                        launchpad.setLightSingle(p, make_shared<ConstLightStatic>(0x48));
                        gametimer.expires_after(chrono::milliseconds(1000));
                        gametimer.async_wait(boost::bind(&game::gametimer_timeout, this, boost::asio::placeholders::error));
                        cout << "You pressed another pad while you had to release them" << endl;
                        gamestate = {RUNNING, INGAME, PROGRESS, level, FAILED, white_pad_to_num(p)};
                    }
                    */
                    if (!press) {
                        if (key_state[midi_channel][pressed_key]) {
                            cout << "Released" << endl;
                            key_state[midi_channel][pressed_key] = 0;
                            fluid_synth.noteoff(midi_channel, pressed_key);
                            launchpad.setLightSingle(p, make_shared<ConstLightStatic>(0x00));
                        }
                        int all_released = true;
                        for (uint8_t b : key_state[midi_channel]) {
                            if (b) {
                                all_released = false;
                                break;
                            }
                        }
                        if (all_released) {
                            cout << debug_prefix() << "Level completed, LED ON" << endl;
                            launchpad.setLightSingle(Pad(8, 8), make_shared<ConstLightStatic>(0x57));
                            gametimer.expires_after(chrono::milliseconds(1000));
                            gametimer.async_wait(boost::bind(&game::gametimer_timeout, this, boost::asio::placeholders::error));
                            gamestate = {RUNNING, INGAME, PROGRESS, level, COMPLETED, GREENON};
                        }
                    }
                }
            }
        }
    }
}

void game::start_guiding(size_t level) {
    start_time = std::chrono::steady_clock::now();
    play_midi_event(midi_file[0][0]);
    wait_for_next_midi_event(1);
    key_state = vector<vector<uint8_t>>(16, vector<uint8_t>(128, 0));
    gamestate = {RUNNING, INGAME, PROGRESS, level, GUIDE, 0};
}

void game::wait_for_next_midi_event(size_t next_event) {
    typedef chrono::steady_clock::duration sysdur;
    typedef chrono::steady_clock::time_point systp;
    doublesec delay(midi_file[0][next_event].seconds);
    sysdur sysdelay (chrono::duration_cast<sysdur>(delay));
    systp expiry = start_time + sysdelay;
    gametimer.expires_at(expiry);
    gametimer.async_wait(boost::bind(&game::gametimer_timeout, this, boost::asio::placeholders::error));
}

void game::start_repeat() {
    assert(gamestate.size() >= 4);
    assert(gamestate[0] == RUNNING);
    assert(gamestate[1] == INGAME);
    assert(gamestate[2] == PROGRESS);
    size_t level = gamestate[3];
    MidiEvent me;
    key_state = vector<vector<uint8_t>>(16, vector<uint8_t>(128, 0));
    size_t e = find_first_note_on(0);
    gamestate = {RUNNING, INGAME, PROGRESS, level, REPEAT, e};
}

size_t game::find_first_note_on(size_t from) {
    MidiEvent me;
    while (1) {
        cout << "find_first_note_on from: " << from << endl;
        cout << "find_first_note_on size: " << midi_file[0].size() << endl;
        assert(from < midi_file[0].size());
        me = midi_file[0][from];
        cout << "find first note on: " << from << endl;
        bool issys = me.isSystem();
        cout << "issys: " << issys << endl;
        if (!issys && me.getChannel() == midi_channel) {
            if (me.isNoteOn()) {
                break;
            }
            else if (me.isTimbre()) {
                cout << debug_prefix() << "timbre" << endl;
                fluid_synth.program_change(me.getChannel(), me.getTimbre());
            }
        }
        ++from;
    }
    return from;
}

void game::gametimer_timeout(const boost::system::error_code &err) {
    typedef chrono::steady_clock::duration sysdur;
    typedef chrono::steady_clock::time_point systp;
    if (!err) {
        for (int x : gamestate) {
            cout << x << ";";
        }
        cout << endl;
        if (gamestate[0] == RUNNING) {
            assert(gamestate.size() >= 2);
            if (gamestate[1] == MENU) {
                assert(gamestate.size() >= 3);
                if (gamestate[2] == LOADING) {
// Uncomment to skip the first levels (debugging)
// #define LPQUICKLAUNCH
#ifdef LPQUICKLAUNCH
                    size_t level = -1;
                    for (size_t on_count = 0; on_count < 15; on_count++) {
                        level++;
                        assert(level < midi_file[0].size());
                        while (midi_file[0][level].isNoteOn() == false || midi_file[0][level].getChannel() != midi_channel) {
                            level++;
                        }
                    }
#else
                    size_t level = 0;
                    while ((assert(level < midi_file[0].size()), midi_file[0][level].isNoteOn() == false) || midi_file[0][level].getChannel() != midi_channel) {
                        level++;
                    }
#endif
                    key_pad_map.shuffle();
                    start_guiding(level);
                }
            }
            else if (gamestate[1] == INGAME) {
                assert(gamestate.size() >= 3);
                if (gamestate[2] == PROGRESS) {
                    assert(gamestate.size() >= 5);
                    size_t level = gamestate[3];
                    if (gamestate[4] == GUIDE) {
                        assert(gamestate.size() >= 6);
                        size_t last_event = gamestate[5];
                        size_t event = last_event + 1;
                        size_t next_event = event + 1;
                        assert(event < midi_file[0].size());
                        cout << "last: " << last_event << "; now: "<< event << "; next: " << next_event << endl;
                        MidiEvent me = midi_file[0][event];
                        if (event <= level) {
                            assert(next_event < midi_file[0].size());
                            play_midi_event(me);
                            wait_for_next_midi_event(next_event);
                            gamestate = {RUNNING, INGAME, PROGRESS, level, GUIDE, event};
                        }
                        else {
                            if (me.isNote()) {
                                int chan = me.getChannel();
                                int key = me.getKeyNumber();
                                if (key_state[chan][key]) {
                                    key_state[chan][key] = 0;
                                    fluid_synth.noteoff(chan, key);
                                }
                                if (chan == midi_channel) {
                                    Pad p = key2pad(key);
                                    cout << debug_prefix() << "X: " << int(p.x) << ", Y: " << int(p.y) << " released" << endl;
                                    launchpad.setLightSingle(p, make_shared<ConstLightStatic>(0x00));
                                    if (key == midi_file[0][level].getKeyNumber()) {
                                        fluid_synth.all_notes_off(-1);
                                        launchpad.reset();
                                        cout << debug_prefix() << "Guide finished" << endl;
                                        if (launchpad.getWhitePressedAny()) {
                                            cout << debug_prefix() << "Waiting for all white pads to be released..." << endl;
                                            gamestate = {RUNNING, INGAME, PROGRESS, level, PRERELEASE};
                                        }
                                        else {
                                            cout << debug_prefix() << "Starting REPEAT phase." << endl;
                                            start_repeat();
                                        }
                                        return;
                                    }
                                }
                            }
                            assert(next_event < midi_file[0].size());
                            wait_for_next_midi_event(next_event);
                            gamestate = {RUNNING, INGAME, PROGRESS, level, GUIDE, event};
                        }
                    }
                    else if (gamestate[4] == FAILED) {
                        assert(gamestate.size() >= 7);
                        Pad p = num_to_white_pad(gamestate[5]);
                        if (gamestate[6] == REDON) {
                            fluid_synth.all_notes_off(-1);
                            launchpad.setLightSingle(p, make_shared<ConstLightStatic>(0x00));
                            cout << debug_prefix() << "RED LED turned off" << endl;
                            gametimer.expires_after(chrono::milliseconds(500));
                            gametimer.async_wait(boost::bind(&game::gametimer_timeout, this, boost::asio::placeholders::error));
                            gamestate[6] = REDOFF;
                        }
                        else {
                            cout << debug_prefix() << "Try again" << endl;
                            launchpad.setSysExTextScroll(true, 10, make_shared<ConstLightStatic>(0x48), "TRY AGAIN");
                            gamestate = {RUNNING, MENU, TRYAGAIN};
                        }
                    }
                    else if (gamestate[4] == COMPLETED) {
                        assert(gamestate.size() >= 6);
                        if (gamestate[5] == GREENON) {
                            launchpad.setLightSingle(Pad(8, 8), make_shared<ConstLightStatic>(0x00));
                            cout << debug_prefix() << "Level completed, LED OFF" << endl;
                            gametimer.expires_after(chrono::milliseconds(500));
                            gametimer.async_wait(boost::bind(&game::gametimer_timeout, this, boost::asio::placeholders::error));
                            gamestate = {RUNNING, INGAME, PROGRESS, level, COMPLETED, GREENOFF};
                        }
                        else {
                            do {
                                level++;
                            }
                            while (level < midi_file[0].size() && (!midi_file[0][level].isNoteOn() || midi_file[0][level].getChannel() != midi_channel));

                            if (level >= midi_file[0].size()) {
                                launchpad.setSysExTextScroll(false, 10, make_shared<ConstLightStatic>(0x20), "WIN");
                                mqtt_client.publish(full_topic_gameevent, gameevent_solved);
                                cout << "Game completed" << endl;
                                gametimer.expires_after(chrono::milliseconds(4000));
                                gametimer.async_wait(boost::bind(&game::gametimer_timeout, this, boost::asio::placeholders::error));
                                gamestate = {RUNNING, INGAME, WIN};
                            }
                            else {
                                cout << "Starting next level" << endl;
                                start_guiding(level);
                            }
                        }
                    }
                }
                else if (gamestate[2] == WIN) {
                    cout << "Back to IDLE" << endl;
                    gamestate = {IDLE};
                }
            }
        }
    }
}

novation::Pad game::key2pad(int key) {
    int h = key_pad_map.f(key - lowkey);
    return Pad(h % 8, h / 8);
}

int game::pad2key(novation::Pad pad) {
    int h = white_pad_to_num(pad);
    return key_pad_map.g(h) + lowkey;
}

void game::play_midi_event(const smf::MidiEvent &e) {
    cout << "play_midi_event" << endl;
    for (smf::uchar c : e) {
        cout << std::hex << int(c) << ";";
    }
    cout << endl;
    if (e.isTimbre()) {
        cout << debug_prefix() << "timbre" << endl;
        fluid_synth.program_change(e.getChannel(), e.getTimbre());
    }
    else if (e.isPitchbend()) {
        cout << debug_prefix() << "pitchbend " << e.getPitchbend() << endl;
        fluid_synth.pitch_bend(e.getChannel(), e.getPitchbend());
    }
    else if (e.isNoteOn()) {
        cout << debug_prefix() << "on" << endl;
        int chan = e.getChannel();
        int key = e.getKeyNumber();
        int vel = e.getVelocity();
        fluid_synth.noteon(chan, key, vel);
        key_state[chan][key] = vel;
        if (e.getChannel() == midi_channel) {
            Pad p = key2pad(e.getKeyNumber());
            cout << "Key number: " << key << endl;
            cout << debug_prefix() << "X: " << int(p.x) << ", Y: " << int(p.y) << endl;
            launchpad.setLightSingle(p, make_shared<ConstLightStatic>(e.getVelocity()));
        }
    }
    else if (e.isNoteOff()) {
        cout << debug_prefix() << "off" << endl;
        int chan = e.getChannel();
        int key = e.getKeyNumber();
        if (key_state[chan][key]) {
            key_state[chan][key] = 0;
            cout << "chan: " << e.getChannel() << "; key: " << key << endl;
            fluid_synth.noteoff(e.getChannel(), e.getKeyNumber());
            if (e.getChannel() == midi_channel) {
                Pad p = key2pad(e.getKeyNumber());
                launchpad.setLightSingle(p, make_shared<ConstLightStatic>(0x00));
            }
        }
    }
    else {
        cout << debug_prefix() << "unknown midi event" << endl;
    }
}

#include <unistd.h>
#include <thread>

void game::reset() {
    fluid_synth.all_notes_off(-1);
    gametimer.cancel();
    launchpad.reset();
    gamestate = {IDLE};
}

bool game::interpret_message(mqtt::const_message_ptr msg) {
    if (super::interpret_message(msg)) {
        return true;
    }
    else {
        if (msg->get_topic() == full_topic_signal) {
            if (msg->to_string() == signal_activate) {
                if (gamestate[0] == IDLE) {
                    cout << debug_prefix() << "Activated." << endl;
                    launchpad.setSysExTextScroll(true, 10, make_shared<ConstLightStatic>(0x10), "START");
                    gamestate = {RUNNING, MENU, STARTSCREEN};
                }
                else {
                    cout << debug_prefix() << "Already running." << endl;
                }
                return true;
            }
            else if (msg->to_string() == signal_reset) {
                if (gamestate[0] == RUNNING) {
                    cout << debug_prefix() << "Reset." << endl;
                    reset();
                }
                else {
                    cout << debug_prefix() << "Already idle." << endl;
                }
                return true;
            }
        }
    }
    return false;
}

void game::clean_exit() {
    reset();
    super::clean_exit();
}

void game::start() {
    launchpad.setCallback(boost::bind(&game::midiCallback, this, _1, _2));
    launchpad.open(port_in, port_out);
    reset();
    if (!midi_file.read(midi_filename)) {
        cout << debug_prefix() << "Cannot read MIDI file." << endl;
        throw midifile_exception();
    }
    midi_file.joinTracks();
    midi_file.doTimeAnalysis();
    cout << debug_prefix() << "MIDI tracks: " << midi_file.getNumTracks() << endl;
    // Check file requirements
    bool key_in_range = true;
    bool has_note_on = false;
    for (size_t i = 0; i < midi_file[0].size(); i++) {
        if (midi_file[0][i].isNote()) {
            int chan = midi_file[0][i].getChannel();
            int key = midi_file[0][i].getKeyNumber();
            if (midi_file[0][i].isNoteOn()) {
                int vel = midi_file[0][i].getVelocity();
                key_state[chan][key] = vel;
                if (chan == midi_channel) {
                    has_note_on = true;
                    if (key < lowkey || key >= lowkey + 64) {
                        key_in_range = false;
                    }
                }
            }
            else if (midi_file[0][i].isNoteOff()) {
                key_state[chan][key] = 0;
            }
        }
    }
    bool finally_all_off = true;
    for (int c = 0; c < 16; c++) {
        for (int k = 0; k < 128; k++) {
            if (key_state[c][k]) {
                finally_all_off = false;
            }
        }
    }
    bool file_invalid = false;
    if (!finally_all_off) {
        cout << debug_prefix() << "Not all notes are finally turned off in the midi file" << endl;
        file_invalid = true;
    }
    if (!has_note_on) {
        cout << debug_prefix() << "Midi game channel does not have any NOTE_ON event" << endl;
        file_invalid = true;
    }
    if (!key_in_range) {
        cout << debug_prefix() << "On the midi game channel some key is not in the range [" << lowkey << "," << (lowkey+64) << ")" << endl;
        file_invalid = true;
    }
    if (file_invalid) {
        throw std::exception();
    }
    try {
        fluid_synth.sfload(soundfont, 1);
    }
    catch (const fluid_exception &e) {
        cout << debug_prefix() << "Cannot load soundfont: " << e.what() << endl;
        throw e;
    }
    try {
    #ifdef __linux__
        fluid_settings.setstr("audio.driver", "alsa");
        fluid_settings.setstr("audio.alsa.device", audio_device);
        fluid_settings.setint("audio.period-size", 512);
        fluid_settings.setint("audio.periods", 8);
    #elif _WIN32
        err = fluid_settings_setstr(ptr, "audio.driver", "dsound");
    #else
    #error "Please define an audio driver for this system!"
    #endif
        fluid_audio_driver = fluid_audio_driver_auto(fluid_settings, fluid_synth);
    }
    catch (const fluid_exception &e) {
        cout << debug_prefix() << "Cannot start fluidsynth: " << e.what() << endl;
        throw e;
    }
    /* Test noteoff
    cout << "Before midi test" << endl;
    cout << "chan 0 on" << endl;
    fluid_synth.noteon(0, 60, 100);
    sleep(1);
    cout << "chan 0 off" << endl;
    fluid_synth.noteoff(0, 60);
    sleep(1);
    cout << "chan 8 on" << endl;
    fluid_synth.noteon(8, 60, 100);
    sleep(1);
    cout << "chan 8 off" << endl;
    fluid_synth.noteoff(8, 60);
    sleep(1);
    cout << "chan 10 on" << endl;
    fluid_synth.noteon(10, 60, 100);
    sleep(1);
    cout << "chan 10 off" << endl;
    fluid_synth.noteoff(10, 60);
    sleep(1);
    cout << "chan 9 on" << endl;
    fluid_synth.noteon(9, 60, 100);
    sleep(1);
    cout << "chan 9 off" << endl;
    fluid_synth.noteoff(9, 60);
    cout << "After midi test" << endl;
    */
    /*
    double last_sec = 0.0;
    cout << "Playing midi file" << endl;
    midi_file.joinTracks();

    MidiEventList& list = midi_file[0];
    for (int i = 0; i < list.size(); i++) {
        std::this_thread::sleep_for(std::chrono::microseconds(long(1000000L * (list[i].seconds - last_sec))));
        play_midi_event(list[i]);
        last_sec = list[i].seconds; */
        /*
        cout << i << ": " << list[i].seconds;
        std::this_thread::sleep_for(std::chrono::microseconds(long(1000000L * (list[i].seconds - last_sec))));
        // usleep(long(1000000) * (list[i].seconds - last_sec));
        if (list[i].isTimbre()) {
            cout << ": timbre";
            fluid_synth_program_change(fluid_synth.get(), list[i].getChannel(), list[i].getP1());
        }
        else if (list[i].isPitchbend()) {
            int pitch = list[i][1] | (list[i][2] << 7);
            cout << ": pitch " << pitch;
            fluid_synth_pitch_bend(fluid_synth.get(), list[i].getChannel(), pitch);
        }
        else if (list[i].isNoteOn()) {
            cout << ": on";
            fluid_synth_noteon(fluid_synth.get(), list[i].getChannel(), list[i].getKeyNumber(), list[i].getVelocity());
            if (list[i].getChannel() == 1) {
                int h = list[i].getKeyNumber() - 32;
                if (h >= 0 && h < 64) {
                    Pad p(h % 8, h / 8);
                    launchpad.setLightSingle(p, make_shared<ConstLightStatic>(list[i].getVelocity()));
                }
            }
        }
        else if (list[i].isNoteOff()) {
            cout << ": off";
            fluid_synth_noteoff(fluid_synth.get(), list[i].getChannel(), list[i].getKeyNumber());
            if (list[i].getChannel() == 1) {
                int h = list[i].getKeyNumber() - 32;
                if (h >= 0 && h < 64) {
                    Pad p(h % 8, h / 8);
                    launchpad.setLightSingle(p, make_shared<ConstLightStatic>(0x00));
                }
            }
        }
        cout << endl;
        last_sec = list[i].seconds;
        */
    // }
    super::start();
}
